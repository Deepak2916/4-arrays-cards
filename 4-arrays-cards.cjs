const arraysCards = [{ "id": 1, "card_number": "5602221055053843723", "card_type": "china-unionpay", "issue_date": "5/25/2021", "salt": "x6ZHoS0t9vIU", "phone": "339-555-5239" },
{ "id": 2, "card_number": "3547469136425635", "card_type": "jcb", "issue_date": "12/18/2021", "salt": "FVOUIk", "phone": "847-313-1289" },
{ "id": 3, "card_number": "5610480363247475108", "card_type": "china-unionpay", "issue_date": "5/7/2021", "salt": "jBQThr", "phone": "348-326-7873" },
{ "id": 4, "card_number": "374283660946674", "card_type": "americanexpress", "issue_date": "1/13/2021", "salt": "n25JXsxzYr", "phone": "599-331-8099" },
{ "id": 5, "card_number": "67090853951061268", "card_type": "laser", "issue_date": "3/18/2021", "salt": "Yy5rjSJw", "phone": "850-191-9906" },
{ "id": 6, "card_number": "560221984712769463", "card_type": "china-unionpay", "issue_date": "6/29/2021", "salt": "VyyrJbUhV60", "phone": "683-417-5044" },
{ "id": 7, "card_number": "3589433562357794", "card_type": "jcb", "issue_date": "11/16/2021", "salt": "9M3zon", "phone": "634-798-7829" },
{ "id": 8, "card_number": "5602255897698404", "card_type": "china-unionpay", "issue_date": "1/1/2021", "salt": "YIMQMW", "phone": "228-796-2347" },
{ "id": 9, "card_number": "3534352248361143", "card_type": "jcb", "issue_date": "4/28/2021", "salt": "zj8NhPuUe4I", "phone": "228-796-2347" },
{ "id": 10, "card_number": "4026933464803521", "card_type": "visa-electron", "issue_date": "10/1/2021", "salt": "cAsGiHMFTPU", "phone": "372-887-5974" }]

/* 

    1. Find all card numbers whose sum of all the even position digits is odd.
    2. Find all cards that were issued before June.
    3. Assign a new field to each card for CVV where the CVV is a random 3 digit number.
    4. Add a new field to each card to indicate if the card is valid or not.
    5. Invalidate all cards issued before March.
    6. Sort the data into ascending order of issue date.
    7. Group the data in such a way that we can identify which cards were assigned in which months.

    Use method chaining to solve #3, #4, #5, #6 and #7.

    NOTE: Do not change the name of this file 
*/

function allCardNumbersWhoseSumOfAllEvenPositionDigitsOdd(arraysCards) {
    const allCards = arraysCards.filter(card => {
        let cardNumberSum = +card.card_number
            .split('')
            .filter((digit, index) => {
                return index % 2 != 0
            })
            .reduce((sum, digit) => {
                return sum + (+digit)
            }, 0)

        return cardNumberSum % 2 != 0
    })
    return allCards

}
// console.log(allCardNumbersWhoseSumOfAllEvenPositionDigitsOdd(arraysCards));

function allCardsIssuedBeforeJune(arraysCards) {
    const cardsBeforeJune = arraysCards.filter(card => {
        let date = card.issue_date.split('/')
        //[0] is month
        return +date[0] < 6
    })
    return cardsBeforeJune
}
// console.log(allCardsIssuedBeforeJune(arraysCards));

function assignCVVToEachCard(arraysCards) {

    let AssainedCards = arraysCards
        .map(card => {
            function randomNumber() {

                let CVV = Math.floor(Math.random() * 1000)
                if (CVV >= 100) {
                    return CVV
                } else {
                    randomNumber()
                }

            }

            return [card, randomNumber()]
        })
        .map(card => {

            card[0]['CVV'] = card[1]
            return card[0]
        })
    return AssainedCards
}
// console.log(assignCVVToEachCard(arraysCards))

function cardIsValidOrNot(arraysCards) {

    const allCards = arraysCards
        .map(card => {

            return [card.card_number.length, card]
        })
        .map(card => {
            if (card[0] < 16) {
                card['vadid_card'] = false
            }
            else {

                card['vadid_caed'] = true
            }
            return card
        })
    return allCards
}
// console.log(cardIsValidOrNot(arraysCards))

function invalidateCardsIssuedBeforeMarch(arraysCards) {
    const allCards = arraysCards.map(card => {
        let month = +card.issue_date.split('/')[0]
        return [card, month]
    })
        .map(card => {
            //[0] is card and [1] is month
            if (card[1] < 3) {
                card[0]['valid_card'] = false
            }
            else {
                card[0]['valid_card'] = true
            }
            return card[0]
        })
    return allCards

}
// console.log(invalidateCardsIssuedBeforeMarch(arraysCards))


function sortDataIntoAscendingOrderOfIssueDate(arraysCards) {
    const sortedCards = arraysCards
        .map(card => {
            let date = card.issue_date.split('/')

            //[1] is day
            if (date[1].length < 2) {
                date[1] = `0${date[1]}`
            }
            date = date.join('/')
            card.issue_date = date
            return card
        })
        .sort((card1, card2) => {
            let date1 = +card1.issue_date.split('/').join('')
            let date2 = +card2.issue_date.split('/').join('')

            return date1 - date2
        })
    return sortedCards
}
// console.log(sortDataIntoAscendingOrderOfIssueDate(arraysCards));

function groupTheDataBaseOnMonth(arraysCards) {
    let allGroupedCards = arraysCards
        .map(card => {
            let month = card.issue_date.split('/')[0]
            return [month, card]
        })
        .reduce((result, card) => {
            //[0] is month and [1] is card
            if (result[card[0]]) {
                result[card[0]].push(card[1])
            } else {
                result[card[0]] = []
                result[card[0]].push(card[1])
            }
            return result
        }, {})
    return allGroupedCards
}
// console.log(groupTheDataBaseOnMonth(arraysCards))